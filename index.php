    <html>

    <head lang="en">
        <meta charset="UTF-8">
        <link rel="stylesheet" href="boot.css">
        <title>Login</title>
    </head>
    <style> .login-panel { margin-top: 150px; }
    </style>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Sign In | Post & Reply System</h3>
                        </div>
                        <div class="panel-body">
                            <form method="post" action="login.php">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Username" name="email" type="username" value="" autofocus />
                                    </div>
                                    <br>
                                    <br>
                                    <input class="btn btn-lg btn-success btn-block" type="submit" value="Login" name="login" />
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    </html>
