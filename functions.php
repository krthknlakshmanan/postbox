<?php
// ini_set('display_errors', 1);
$user_id = $_REQUEST['userid'];
// connect to database
global $db;
include "db_conection.php";
if (getUsernameById($user_id) != $_REQUEST['username'] && getIdByUsername($_REQUEST['username']) != $user_id) {
    header("Location: index.php");
    exit;
}
// get post with id 1 from database
$post_query_result = mysql_query("SELECT * FROM posts WHERE id='1'"); #FIXME
$post = mysql_fetch_assoc($post_query_result);

// Get all comments from database
$comments_query_result = mysql_query(sprintf("SELECT * FROM comments WHERE post_id='%s' ORDER BY created_at DESC", mysql_real_escape_string($post['id'])));
while ($row = mysql_fetch_assoc($comments_query_result)) {
    $comments[] = $row;
}
// Receives a user id and returns the username
function getUsernameById($id)
{
    global $db;
    $result = mysql_query(sprintf("SELECT username FROM users WHERE id='%s' LIMIT 1", mysql_real_escape_string($id)));
    // return the username
    return mysql_fetch_assoc($result)['username'];
}

// Receives a user id and returns the username
function getIdByUsername($uname)
{
    global $db;
    $result = mysql_query(sprintf("SELECT id FROM users WHERE username='%s' LIMIT 1", mysql_real_escape_string($uname)));
    // return the username
    return mysql_fetch_assoc($result)['id'];
}
// Receives a comment id and returns the username
function getRepliesByCommentId($id)
{
    global $db;
    $result = mysql_query(sprintf("SELECT * FROM replies WHERE comment_id='%s' ORDER BY created_at DESC", mysql_real_escape_string($id)));
    while ($row = mysql_fetch_assoc($result)) {
        $replies[] = $row;
    }
    return $replies;
}
// Receives a post id and returns the total number of comments on that post
function getCommentsCountByPostId($post_id)
{
    global $db;
    $result = mysql_query("SELECT COUNT(*) AS total FROM comments");
    $data = mysql_fetch_assoc($result);
    return $data['total'];
}

if (isset($_POST['comment_posted'])) {
    global $db;
    $comment_text = $_POST['comment_text'];
    $sql = sprintf("INSERT INTO comments (post_id, user_id, body, created_at, updated_at) VALUES ('%s', '%s', '%s', now(), now())",
        1,
        mysql_real_escape_string($user_id),
        mysql_real_escape_string($comment_text)
    );
    // echo $sql;die;
    $result = mysql_query($sql);
    $inserted_id = mysql_insert_id();
    $res = mysql_query(sprintf("SELECT * FROM comments WHERE id='%s'", mysql_real_escape_string($inserted_id)));
    $inserted_comment = mysql_fetch_assoc($res);
    if ($result) {
        $comment = "<div class='comment clearfix'>
					<img src='https://cdn-icons-png.flaticon.com/512/149/149071.png' alt='' class='profile_pic'>
					<div class='comment-details'>
						<span class='comment-name'>" . getUsernameById($inserted_comment['user_id']) . "</span>
						<span class='comment-date'>" . date('F j, Y ', strtotime($inserted_comment['created_at'])) . "</span>
						<p>" . $inserted_comment['body'] . "</p>
						<a class='reply-btn' href='#' data-id='" . $inserted_comment['id'] . "'>reply</a>
					</div>
					<!-- reply form -->
					<form action='post_details.php?username=" . trim($_REQUEST['username']) . "&userid=" . $_REQUEST['userid'] . "' class='reply_form clearfix' id='comment_reply_form_" . $inserted_comment['id'] . "' data-id='" . $inserted_comment['id'] . "' >
						<textarea class='form-control' name='reply_text' id='reply_text' cols='30' rows='2'></textarea>
						<button class='btn btn-primary btn-xs pull-right submit-reply'>Submit reply</button>
					</form>
				</div>";
        $comment_info = array(
            'comment' => $comment,
            'comments_count' => getCommentsCountByPostId(1),
        );
        echo json_encode($comment_info);
        exit();
    } else {
        echo "error";
        exit();
    }
}
if (isset($_POST['reply_posted'])) {
    global $db;
    $reply_text = $_POST['reply_text'];
    $comment_id = $_POST['comment_id'];
    $sql = sprintf("INSERT INTO replies (user_id, comment_id, body, created_at, updated_at) VALUES ('%s', '%s', '%s', now(), now())",
        mysql_real_escape_string($user_id),
        mysql_real_escape_string($comment_id),
        mysql_real_escape_string($reply_text)
    );
    $result = mysql_query($sql);
    $inserted_id = mysql_insert_id();
    $res = mysql_query(sprintf("SELECT * FROM replies WHERE id='%s'", mysql_real_escape_string($inserted_id)));
    $inserted_reply = mysql_fetch_assoc($res);
    if ($result) {
        $reply = "<div class='comment reply clearfix'>
					<img src='https://cdn-icons-png.flaticon.com/512/149/149071.png' alt='' class='profile_pic'>
					<div class='comment-details'>
						<span class='comment-name'>" . getUsernameById($inserted_reply['user_id']) . "</span>
						<span class='comment-date'>" . date('F j, Y ', strtotime($inserted_reply['created_at'])) . "</span>
						<p>" . $inserted_reply['body'] . "</p>
						<a class='reply-btn' href='#' data-id=" . $inserted_reply['comment_id'] . ">reply</a>
					</div>
				</div>";
        echo $reply;
        exit();
    } else {
        echo "error";
        exit();
    }
}
